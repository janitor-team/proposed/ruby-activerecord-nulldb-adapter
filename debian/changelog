ruby-activerecord-nulldb-adapter (0.8.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.
  * Fix field name case in debian/control (Rules-requires-root =>
    Rules-Requires-Root).

  [ Daniel Leidert ]
  * d/control (Standards-Version): Bump to 4.6.0.
    (Build-Depends): Bump to ruby-activerecord >= 2:6.1 (closes: #996119).
    (Depends): Add ${ruby:Depends}.
  * d/copyright: Add Upstream-Contact field.
    (Copyright): Add team.
  * d/rules: Use gem installation layout and install changelog.
  * d/upstream/metadata: Add Archive and Changelog fields.

 -- Daniel Leidert <dleidert@debian.org>  Sun, 21 Nov 2021 02:42:17 +0100

ruby-activerecord-nulldb-adapter (0.4.0-1) unstable; urgency=medium

  * Team upload.

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Lucas Kanashiro ]
  * New upstream version 0.4.0
  * Bump debhelper compatibility level to 12
  * Update VCS fields to point to salsa
  * d/copyright: use secure url in Format field
  * Drop patches applied by upstream
  * d/watch: update to gemwatch.debian.net
  * Declare compliance with Debian Policy 4.5.0
  * d/control: do not require root to build
  * Do not runtime depend on ruby interpreter
  * Enable autodep8 tests for ruby packages

 -- Lucas Kanashiro <lucas.kanashiro@canonical.com>  Thu, 06 Feb 2020 08:24:15 -0300

ruby-activerecord-nulldb-adapter (0.3.2-1) unstable; urgency=medium

  * Team upload.

  [ Sebastien Badia ]
  * Fix FTBFS bug with a upstream patch (Closes: #759918)
  * d/control:
    - Bump Standards-Version (no changes)
    - Wrap and sort, switch to cgit and HTTPS for homepage/vcs-browser

  [ Lucas Albuquerque Medeiros de Moura ]
  * New upstream release.
  * Removing patches already applied on new upstream version.
  * Update how package test were executed.
  * Add patch to fix tests for activerecord 4.2.
  * debian/rules: Add check-dependencies command.

 -- Lucas Albuquerque Medeiros de Moura <lucas.moura128@gmail.com>  Sat, 05 Mar 2016 18:21:21 -0300

ruby-activerecord-nulldb-adapter (0.3.1-1) unstable; urgency=medium

  * Initial release (Closes: #752371)

 -- Cédric Boutillier <boutil@debian.org>  Wed, 09 Jul 2014 22:09:29 +0200
